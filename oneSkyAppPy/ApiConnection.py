import requests
import json
import hashlib
import time

API_URL = 'https://platform.api.onesky.io/1/'
API_KEY = '3M5po00FyMWnc8wbpaGsUufPoPFsMIIG'
API_SECRET = 'm5JPvM4LqHVrZzlj9rNFcTXX43J0d7dq'


class ApiConnection:
    """OneSkyApp API implementation"""
    def __init__(self, project_id):
        self.api_url = API_URL
        self.api_key = API_KEY
        self.api_secret = API_SECRET
        self.project_id = project_id
        self.timestamp = str(int(time.time()))
        self.dev_hash = self._get_dev_hash()
        self.parameters = {
            'api_key' : self.api_key,
            'timestamp' : self.timestamp,
            'dev_hash' : self.dev_hash
            }

    # Private methods
    def _get_dev_hash(self):
        dev_hash = hashlib.md5()
        dev_hash.update(self.timestamp.encode('utf-8'))
        dev_hash.update(self.api_secret.encode('utf-8'))
        return dev_hash.hexdigest()

    def _do_request(self, params, sub_url):
        url = self.api_url + sub_url
        response = requests.get(url, params=params)
        return response.json()

    # Public methods
    def project_properties(self):
        sub_url = 'projects/{}'.format(self.project_id)
        response = self._do_request(self.parameters, sub_url)
        return response

    def upload_file(self, file):
        return 5
