## IDEA
the main idea is create a script that parse a whole project (all folders and files)
and from this make up an *.po file which may be uploaded to OneSkyApp through the API
since this service only accept a few formats it could be another one like strins file.
This file will be uploaded and OneSkyapp will take care of add all new strings or update
the existing ones. This program also must be provide a way to download all translations
in *.po files and in the correct folder of the project.

### Parsing inside a project looking for I18N functions ( language support )
Using *REGULAR EXPRESSIONS* the application will find all translation functions like:
 - `__()`
 - `_e()`
 - `_x()`
 - In this [link][https://codex.wordpress.org/L10n] there are more.
 
 Thus some filesystem path library as `pathlib` must be used.